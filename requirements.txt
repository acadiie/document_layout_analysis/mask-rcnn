numpy>=1.23.5
opencv_python>=4.7
Pillow~=9.4
pytorch_lightning~=1.9
torch~=1.13
torchvision~=0.14